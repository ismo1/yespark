FactoryGirl.define do
  factory :parking do
      name "Olivier Métra"
      address  "15 rue Olivier Métra"
      available  false
      has_camera  false
      has_watchman  true
      zip_code  "75020"
      city  "Paris"
      main_picture  "https://yespark.imgix.net/Parking-olivier-metra-1901.JPG?ixlib=rails-2.1.0&auto=enhance&fit=crop&w=450&h=300"
      price_month  7200.0
      district  "20"
  end
end
