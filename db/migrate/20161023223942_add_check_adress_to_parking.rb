class AddCheckAdressToParking < ActiveRecord::Migration[5.0]
  def change
    add_column :parkings, :check_address, :boolean, :default => false
  end
end
