class AddCoordonneeToParking < ActiveRecord::Migration[5.0]
  def change
    add_column :parkings, :lat, :float
    add_column :parkings, :lng, :string
    add_column :parkings, :district, :string
  end
end
