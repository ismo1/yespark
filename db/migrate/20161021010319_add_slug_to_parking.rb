class AddSlugToParking < ActiveRecord::Migration[5.0]
  def change
    add_column :parkings, :slug, :string
    add_index :parkings, :slug, unique: true
  end
end
