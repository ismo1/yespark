class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :set_map_markers
  before_filter :set_page_controller

  include ApplicationHelper

  private

  def set_map_markers
    return unless is_page_controller?
    @map_parkings = Parking.all
    @hash = Gmaps4rails.build_markers(@map_parkings) do |parking, marker|
      marker.lat parking.lat
      marker.lng parking.lng
      marker.title parking.name
      marker.infowindow "<ul><li>Name: #{parking.name}</li> <li>Address: #{parking.address}</li> <li>Zip_code: #{parking.zip_code}</li> <li>City: #{parking.city}</li> <li>Price: #{parking.price_month}</li><li><a href='#{parking_path(parking)}'><img src='#{parking.main_picture}' alt='Picture' height='auto' width='100px' /></a></li></ul>"
    end
  end

  def set_page_controller
    is_page_controller?
  end

end
