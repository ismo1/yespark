class ParkingsController < ApplicationController
  before_action :set_parking, only: [:show, :edit, :update, :destroy]

  # GET /parkings
  def index
    @parkings = Parking.all
  end

  # GET /parkings/1
  def show
    if request.path != parking_path(@parking)
      redirect_to @parking, status: :moved_permanently
    end
  end

  # GET /parkings/new
  def new
    @parking = Parking.new
  end

  # GET /parkings/1/edit
  def edit
  end

  # POST /parkings
  def create
    @parking = Parking.new(parking_params)

    if @parking.save
      redirect_to @parking, notice: 'Parking was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /parkings/1
  def update
    if @parking.update(parking_params)
      redirect_to @parking, notice: 'Parking was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /parkings/1
  def destroy
    @parking.destroy
    redirect_to parkings_url, notice: 'Parking was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parking
      @parking = Parking.friendly.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def parking_params
      params.require(:parking).permit(:name, :address, :available, :has_camera, :has_watchman, :zip_code, :city, :main_picture, :price_month)
    end
end
