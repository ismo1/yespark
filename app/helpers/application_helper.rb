module ApplicationHelper
  def is_page_controller?
    @is_page_controller ||= controller_name == 'pages'
  end
end
