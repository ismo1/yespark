# == Schema Information
#
# Table name: parkings
#
#  id           :integer          not null, primary key
#  name         :string
#  address      :string
#  available    :boolean
#  has_camera   :boolean
#  has_watchman :boolean
#  zip_code     :string
#  city         :string
#  main_picture :string
#  price_month  :money
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  lat          :float
#  district     :string
#  slug         :string
#

class Parking < ApplicationRecord
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :history, :finders]

  after_save :geocode_address
  before_validation :check_changes
  before_validation :set_district, :on => :create
  validates :name, :slug, :city, presence: true

  def slug_candidates
    [
      :name,
      [:name, :city]
    ]
  end

  private

  def set_district
    return unless city_changed? || zip_code_changed?
    self.district = self.city.upcase == "PARIS" ? self.zip_code.split(//).last(2).join : "O"
  end

  def check_changes
    self.check_address = address_changed? || zip_code_changed? || city_changed? || false
  end

  def geocode_address
    return unless self.check_address
    begin
      geo = Geokit::Geocoders::MultiGeocoder.geocode("#{address} #{zip_code} #{city}")
    rescue => e
      Rails.logger.error e.message
    end
    unless geo.success
      message = "Could not Geocode this : #{address} #{zip_code} #{city}"
      errors.add(:base, message)
      Rails.logger.error message
    else
      self.lat, self.lng = geo.lat,geo.lng
      self.city = geo.city
      self.check_address = true
    end
    self.save!
  end
  handle_asynchronously :geocode_address
end
